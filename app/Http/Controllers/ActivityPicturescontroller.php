<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Picture;
use App\Picture_id;

class ActivityPicturescontroller extends Controller
{
    //
    public function index()
    {
        $picture = Picture::all();
        return view('activity.activity',compact('picture'));
    }
    public function show($id)
    {
        $picture = Picture::find($id);
        $picture1 = Picture::find($id)->picture;
        return view('activity.activitydetail',compact('picture','picture1'));
    }
}
