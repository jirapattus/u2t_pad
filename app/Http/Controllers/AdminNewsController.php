<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\News;

class AdminNewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('created_at', 'desc')->get();
        // dd($picture);
        return view('admin.news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          // validate the data
          $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'title'  => 'required',
            'content'  => 'required',
        ]);
        // store in the database
        $news = new News;
        if ($request->file('photo')) {
            $imagePath = $request->file('photo');
            $imageName = time().'-'.$imagePath->getClientOriginalName();
    
            $path = $request->file('photo')->storeAs('uploads/news', $imageName,'public');
          }
          $news->id_admin = Auth::user()->name; 
          $news->name = $imageName;
          $news->path = '/storage/'.$path;
          $news->title = $request->title;
          $news->content = $request->content;
          $news->save();
          return redirect('admin/news')->with('success', 'บันทึกข่าวสำเร็จ');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return view('admin.news.edit', compact("news"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          // validate the data
       $request->validate([
        'title'  => 'required',
        'content'  => 'required',
    ]);
    // store in the database
      $news = News::find($id);
      $news->title = $request->title;
      $news->content = $request->content;
      $news->update();
      return redirect('admin/news')->with('success', 'แก้ไขข่าวสำเร็จ');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        unlink(storage_path('/app/public/uploads/news/'.$news->name));
        $news->delete();
        return redirect()->route('admin.news.index')->with('success', 'ลบข่าวสำเร็จ'); 
    }
    public function updateimage(Request $request, $id)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
          ]);

          $news = News::find($id);
          unlink(storage_path('/app/public/uploads/news/'.$news->name));
          if ($request->file('image')) {
            $imagePath = $request->file('image');
            $imageName = time().'-'.$imagePath->getClientOriginalName();
  
            $path = $request->file('image')->storeAs('uploads/news', $imageName,'public');
          }
  
          $news->name = $imageName;
          $news->path = '/storage/'.$path;
          $news->update();
          return back()->with('success', 'แก้ไขรูปภาพสำเร็จ');
    }
}
