<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Picture;
use App\Picture_id;
use Illuminate\Support\Str;

class AdminPictureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $picture = Picture::orderBy('created_at', 'desc')->get();
        // dd($picture);

        return view('admin.picture.index',compact('picture'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.picture.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'title'=>'required',
            'content'=>'required',

        ]);
        $picture = new Picture;
  
          if ($request->file('image')) {
            $imagePath = $request->file('image');
            $imageName = time().'-'.$imagePath->getClientOriginalName();
  
            $path = $request->file('image')->storeAs('uploads/picture', $imageName,'public');
          }
          $picture->title = $request->title;
          $picture->content = $request->content;
          $picture->name = $imageName;
          $picture->path = '/storage/'.$path;
          $picture->save();
          return redirect('admin/picture')->with('success', 'บันทึกรูปภาพสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $picture = Picture::find($id);
        return view('admin.picture.edit', compact("picture"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'content'=>'required',
        ]);
        $picture = Picture::find($id);
        $picture->title = $request->title;
        $picture->content = $request->content;
        $picture->update(); 
          return redirect('admin/picture')->with('success', 'แก้ไขสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $picture = Picture::find($id);
        // dd($picture);
         $picture1 = Picture::find($id)->picture;
          foreach ($picture1 as $key => $picture1) {
            unlink(storage_path('/app/public/uploads/picture/'.$picture1->name));
        }
        $picture->delete();
        return back()->with('success', 'ลบรูปภาพสำเร็จ');
    }

    public function image($id)
    {
        $picture = Picture::find($id);
        $picture1 = Picture::find($id)->picture;
        return view('admin.picture.image',compact('picture','picture1'));
    }
    
    public function addimage(Request $request,$id)
    {
        $request->validate([
            'files' => 'required',
          ]);
          if ($request->hasfile('files')) {
              $files = $request->file('files');
  
              foreach($files as $file) {
                  $name = time() . '_' . Str::random(25) . '.' . $file->getClientOriginalName();
                  $path = $file->storeAs('uploads/picture', $name, 'public');
                  $picture = Picture::find($id);
  
                  Picture_id::create([
                      'id_pic' => $picture->id,
                      'name' => $name,
                      'path' => '/storage/'.$path
                    ]);
              }
           }
           return back()->with('success', 'เพิ่มรูปภาพสำเร็จ');
    }

    public function deleteimage($id)
    {
        $picture_ids = Picture_id::findOrFail($id);
        unlink(storage_path('/app/public/uploads/picture/'.$picture_ids->name));
        $picture_ids->delete();
        return back()->with('success', 'ลบรูปภาพสำเร็จ');
      
    }

    public function updateimage(Request $request, $id)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
          ]);

          $picture = Picture::find($id);
          unlink(storage_path('/app/public/uploads/picture/'.$picture->name));
          if ($request->file('image')) {
            $imagePath = $request->file('image');
            $imageName = time().'-'.$imagePath->getClientOriginalName();
  
            $path = $request->file('image')->storeAs('uploads/picture', $imageName,'public');
          }
  
          $picture->name = $imageName;
          $picture->path = '/storage/'.$path;
          $picture->update();
          return back()->with('success', 'แก้ไขรูปภาพสำเร็จ');
    }

}
