<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poster;
use Illuminate\Support\Facades\Storage;


class AdminPosterController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $poster = Poster::orderBy('created_at', 'desc')->get();
        return view('admin.poster.index', compact("poster"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.poster.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'poster' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
          ]);
  
          $poster = new Poster;
  
          if ($request->file('poster')) {
            $imagePath = $request->file('poster');
            $imageName = time().'-'.$imagePath->getClientOriginalName();
  
            $path = $request->file('poster')->storeAs('uploads/poster', $imageName,'public');
          }
  
          $poster->name = $imageName;
          $poster->path = '/storage/'.$path;
          $poster->save();
  
          return redirect('admin/poster')->with('success', 'บันทึกรูปภาพสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $poster = Poster::find($id);
        return view('admin.poster.edit', compact("poster"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $poster = Poster::find($id);
      $destinationPath = $poster->path;
        unlink(storage_path('/app/public/uploads/poster/'.$poster->name));
      if ($request->file('poster')) {
        $imagePath = $request->file('poster');
        $imageName = time().'-'.$imagePath->getClientOriginalName();

        $path = $request->file('poster')->storeAs('uploads/poster', $imageName,'public');
      }

      $poster->name = $imageName;
      $poster->path = '/storage/'.$path;
      $poster->update();

      return redirect('admin/poster')->with('success', 'แก้ไขรูปภาพสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $poster = Poster::find($id);
      unlink(storage_path('/app/public/uploads/poster/'.$poster->name));
      $poster->delete();
      return redirect()->route('admin.poster.index')->with('success', 'ลบสำเร็จ');

    }
}
