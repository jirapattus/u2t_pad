<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\Store_id;
use Illuminate\Support\Str;

class AdminStoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $store = Store::orderBy('created_at', 'desc')->get();
        return view('admin.store.index', compact("store"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'price'  => 'required',
            'category'  => 'required',
            'phone'  => 'required',
            'facebook'  => 'required',
            'line'  => 'required',
            'details'  => 'required',

          ]);
  
          $store = new Store;
  
          if ($request->file('image')) {
            $imagePath = $request->file('image');
            $imageName = time().'-'.$imagePath->getClientOriginalName();
  
            $path = $request->file('image')->storeAs('uploads/store', $imageName,'public');
          }
          $store->price = $request->price;
          $store->category = $request->category;
          $store->phone = $request->phone;
          $store->facebook = $request->facebook;
          $store->line = $request->line;
          $store->details = $request->details;
          $store->name = $imageName;
          $store->path = '/storage/'.$path;
          $store->save();
  
          return redirect('admin/store')->with('success', 'บันทึกรูปภาพสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::find($id);
        return view('admin.store.edit', compact("store"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          // validate the data
       $request->validate([
        'price'  => 'required',
        'category'  => 'required',
        'phone'  => 'required',
        'facebook'  => 'required',
        'line'  => 'required',
        'details'  => 'required',
    ]);
    // store in the database
    $store = Store::find($id);
      $store->price = $request->price;
      $store->category = $request->category;
      $store->phone = $request->phone;
      $store->facebook = $request->facebook;
      $store->line = $request->line;
      $store->details = $request->details;
      $store->update();
      return redirect('admin/store')->with('success', 'แก้ไขสินค้าสำเร็จ');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Store::find($id);
        
        $store1 = Store::find($id)->picture;
          foreach ($store1 as $key => $store1) {
            unlink(storage_path('/app/public/uploads/store/'.$store1->name));
            $store1->delete();
        }
        unlink(storage_path('/app/public/uploads/store/'.$store ->name));
        $store->delete();
        return back()->with('success', 'ลบรูปภาพสำเร็จ');
    }

    public function image1($id)
    {
         $store = Store::find($id);
         $picture = Store::find($id)->picture;
        return view('admin.store.image',compact('store','picture'));
    }
    public function addimage(Request $request,$id)
    {
        $request->validate([
            'files' => 'required',
          ]);
          if ($request->hasfile('files')) {
              $files = $request->file('files');
  
              foreach($files as $file) {
                  $name = time() . '_' . Str::random(25) . '.' . $file->getClientOriginalName();
                  $path = $file->storeAs('uploads/store', $name, 'public');
                  $store = Store::find($id);
  
                  Store_id::create([
                      'id_pic' => $store->id,
                      'name' => $name,
                      'path' => '/storage/'.$path
                    ]);
              }
           }
           return back()->with('success', 'เพิ่มรูปภาพสำเร็จ');
    }

    public function updateimage(Request $request, $id)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
          ]);

          $store = Store::find($id);
          unlink(storage_path('/app/public/uploads/store/'.$store->name));
          if ($request->file('image')) {
            $imagePath = $request->file('image');
            $imageName = time().'-'.$imagePath->getClientOriginalName();
  
            $path = $request->file('image')->storeAs('uploads/store', $imageName,'public');
          }
  
          $store->name = $imageName;
          $store->path = '/storage/'.$path;
          $store->update();
          return back()->with('success', 'แก้ไขรูปภาพสำเร็จ');
    }

    public function deleteimage($id)
    {
        $store_id = Store_id::findOrFail($id);
        unlink(storage_path('/app/public/uploads/store/'.$store_id->name));
        $store_id->delete();
        return back()->with('success', 'ลบสินค้าสำเร็จ');
      
    }
    
}
