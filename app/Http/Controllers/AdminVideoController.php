<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class AdminVideoController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $video = Video::orderBy('created_at', 'desc')->get();
        return view('admin.video.index', compact("video"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.video.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $request->validate([
        'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        'linkvideo'  => 'required',
        'title'  => 'required',
        'content'  => 'required',
    ]);
    // store in the database
    $video = new Video;
    if ($request->file('photo')) {
        $imagePath = $request->file('photo');
        $imageName = time().'-'.$imagePath->getClientOriginalName();

        $path = $request->file('photo')->storeAs('uploads/video', $imageName,'public');
      }

      $video->name = $imageName;
      $video->path = '/storage/'.$path;
      $video->linkvideo = $request->linkvideo;
      $video->title = $request->title;
      $video->content = $request->content;
      $video->save();
      return redirect('admin/video')->with('success', 'บันทึกวิดีโอสำเร็จ');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::find($id);
        return view('admin.video.edit', compact("video"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // validate the data
       $request->validate([
        'linkvideo'  => 'required',
        'title'  => 'required',
        'content'  => 'required',
    ]);
    // store in the database
    $video = Video::find($id);
      $video->linkvideo = $request->linkvideo;
      $video->title = $request->title;
      $video->content = $request->content;
      $video->update();
      return redirect('admin/video')->with('success', 'แก้ไขวิดีโอสำเร็จ');  
    }

    public function updateimage(Request $request, $id)
    {
        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
          ]);

          $video = Video::find($id);
          unlink(storage_path('/app/public/uploads/video/'.$video->name));
          if ($request->file('photo')) {
            $imagePath = $request->file('photo');
            $imageName = time().'-'.$imagePath->getClientOriginalName();
  
            $path = $request->file('photo')->storeAs('uploads/video', $imageName,'public');
          }
  
          $video->name = $imageName;
          $video->path = '/storage/'.$path;
          $video->update();
          return back()->with('success', 'แก้ไขรูปภาพสำเร็จ');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $video = Video::find($id);
      unlink(storage_path('/app/public/uploads/video/'.$video->name));
      $video->delete();
      return redirect()->route('admin.video.index')->with('success', 'ลบวิดีโอสำเร็จ'); 
    }
}
