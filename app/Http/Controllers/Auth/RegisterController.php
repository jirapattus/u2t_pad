<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function register(Request $request)
    {
        Validator::make($request->all(), [
            'tname' => ['required', 'string', 'max:255'],
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'work' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'image' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg']
        ])->validate();

        $data = $request->all();
        $data_create = [
            'tname' => $data['tname'],
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'work' => $data['work'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ];
        if (!empty($data['image'])) {
            # code...
            $image = request()->file('image');
            $name = time() . '_' . Str::random(25) . '.' . $image->getClientOriginalExtension();
            $folder = '/uploads/avatar/';
            $filePath = $folder . $name;
            $file = $image->storeAs($folder, $name, 'public');
            $data_create = array_merge($data_create, ['name' => $name]);
            $data_create = array_merge($data_create, ['path' => '/storage' . $filePath]);
        }
        // dd($data_create);

        $user = User::create($data_create);
        return redirect()->to('/admin/user');
    }

    
}
