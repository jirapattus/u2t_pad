<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class Newscontroller extends Controller
{
    //
    public function index()
    {
        $news = News::all();
        return view('news.news',compact('news'));
    }

    public function show($id)
    {
        $news = News::find($id);
        
        return view('news.newsdetail', compact("news"));
    }
}
