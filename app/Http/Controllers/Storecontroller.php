<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\Store_id;

class Storecontroller extends Controller
{
    //
    public function index()
    {
        $store = Store::all();
        return view('store.store', compact("store"));
    }
    public function show($id)
    {
         $store = Store::find($id);
         $picture = Store::find($id)->picture;
         return view('store.storedetail',compact('store','picture'));
    }
}
