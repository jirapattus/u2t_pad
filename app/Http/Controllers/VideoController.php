<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends Controller
{
    //
    public function index()
    {
        $video = Video::all();
        return view('video.video', compact("video"));
    }
    public function show($id)
    {
        $video = Video::find($id);
        
        return view('video.show', compact("video"));
    }

}
