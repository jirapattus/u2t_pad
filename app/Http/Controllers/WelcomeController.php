<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poster;
use App\Video;
use App\Store;
use App\News;
use App\Picture;

class WelcomeController extends Controller
{
    public function index()
    {
        $poster = Poster::all();
        $video = Video::all();
        $store = Store::all();
        $news = News::all();
        $picture = Picture::all();
        return view('welcome', compact('poster','video','store','news','picture'));
    }
}
