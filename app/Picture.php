<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $fillable = [
        'id',
        'name', 
        'path',
        'title', 
        'content'
    ];
    
    public function picture(){
        return $this->hasMany(Picture_id::class, 'id_pic', 'id');
        }
}

