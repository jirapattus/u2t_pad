<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture_id extends Model
{
    protected $fillable = [
        'id_pic',
        'name', 
        'path'
    ];

}
