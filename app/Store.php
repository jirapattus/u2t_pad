<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = [
        'id',
        'price', 
        'category',
        'phone',
        'facebook',
        'line',
        'details',
        'name',
        'path',
    ];

    public function picture(){
        return $this->hasMany(Store_id::class, 'id_pic', 'id');
        }
}
