<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store_id extends Model
{
    protected $fillable = [
        'id_pic',
        'name', 
        'path'
    ];
}
