<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'tname' => 'นาย',
            'fname' => 'จิรพัฒน์',
            'lname' => 'ตะโพธิ์',
            'work' => 'พนักงาน',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'role' => '1',
            'name' => 'person.png',
            'path' => '/image/person.png',
        ]);
    }
}
