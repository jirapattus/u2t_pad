@extends('layouts.app')

@section('content')
<main id="main">
    <!-- ======= Blog Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>รูปภาพกิจกรรม</h2>
          <ol>
            <li><a href="{{url('/')}}">หน้าแรก</a></li>
            <li><a>รูปภาพกิจกรรม</a></li>
          </ol>
      </div>
    </section><!-- End Blog Section -->

    <section  class="service-details">
      <div class="container">

        <div class="row">
          @foreach ($picture as $key => $item)
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="{{ $item->path }}" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a>{{ $item->title }}</a></h5>
                <p class="card-text">{{Str::limit($item->content, 200, $end='.......')}}</p>
                <div class="font" class="read-more"><a href="{{ route('activity.show',[$item->id]) }}"><i class="bi bi-arrow-right"></i> ดูเพิ่มเติม</a></div>
              </div>
            </div>
          </div>
          @endforeach
        </div>

      </div>
    </section><!-- End Service Details Section -->

</main>


    
@endsection
