@extends('layouts.app')

@section('content')

<main id="main">
    <!-- ======= Contact Section ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
            <h2>รูปภาพกิจกรรมเพิ่มเติม</h2>
            <ol>
            <li><a href="{{url('/')}}">หน้าแรก</a></li>
            <li><a href="{{url('/activity')}}">รูปภาพกิจกรรม</a></li>
            <li>รูปภาพกิจกรรมเพิ่มเติม</li>
          </ol>
            </div>

        </div>
    </section><!-- End Contact Section -->

    <section class="portfolio">
      <div class="container">

        <div class="row">
          
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
          <div class="section-title"></br>
            <h2>{{ $picture->title }}</h2>
            <p>{{ $picture->content }}</p>
          </div>
          @foreach ($picture1 as $key => $pictures)
        <div class="col-lg-4 col-md-6 portfolio-wrap filter-web">
            <div class="portfolio-item">
              <img src="{{$pictures->path}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                <a href="{{$pictures->path}}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="ขยายรูปภาพ"><i class="bx bx-plus"></i></a>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>

      </div>
    </section><!-- End Portfolio Section -->
</main>
@endsection
    