@extends('layouts.admin.app')
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">จัดการสินค้า</h1>
    <a href="{{route("admin.news.create")}}" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i
            class="fas fa-download fa-sm text-white-50"></i> เพิ่มข้อมูล</a>
</div>
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รูปข่าว</th>
                            <th>หัวเรื่อง</th>
                            <th>เนื้อหา</th>
                            <th>วันที่โพสต์</th>
                            <th>จัดการ</th>
                            <th>จัดการ</th>

                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($news as $key => $news)   
                        <tr>
                            <td style="width:5%">{{ $key+1}}</td>
                            <td><img src="{{$news->path}}" style="height:200px; width:300px" /></td>
                            <td>{{$news->title}}</td>
                            <td>{{$news->content}}</td>
                            <td>{{$news->created_at}}</td>
                            <td style="width:5%" align="center" ;>
                                <a href="{{ route('admin.news.edit',[$news->id]) }}"
                                    class="btn btn-warning btn-circle">
                                    <i class="fas fa-exclamation-triangle"></i>
                                </a>
                            </td>
                            <td style="width:5%" align="center">
                                <form class="delete_form" action="{{route('admin.news.destroy',$news->id)}}" method="post">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger btn-circle" type="submit" name="" value="Delete"><i
                                            class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                     @endforeach   
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@endsection
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).ready(function () {
        $('.delete_form').on('submit', function () {
            if (confirm("คุณต้องการลบข้อมูลหรือไม่")) {
                return true;
            } else {
                return false;
            }

        });
    });

</script>
