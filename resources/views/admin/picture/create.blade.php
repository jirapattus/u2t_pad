@extends('layouts.admin.app')
@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เพิ่มโปสเตอร์/ประชาสัมพันธ์</h6>
        </div>
        <div class="container py-3">
            <img id="output" class="img-fluid py-2" src="{{asset('image/Ex1poster.jpg')}}" alt="Responsive image" />
            <form method="post" action="{{ route('admin.picture.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">รูปปก</label>
                    <div class="col-sm-10">
                        <input type="file" name="image"
                            class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" accept="image/*"
                            onchange="loadFile(event)" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">หัวเรื่อง</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" value="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">เนื่อหา</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="content"
                            rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group row py-3 ">

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
