@extends('layouts.admin.app')
@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">แก้ไขโพสต์รูปภาพ</h6>
        </div>
        <div class="container py-3">
            <img class="img-fluid py-2" src="{{asset($picture->path)}}" alt="Responsive image" />
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">รูปปกวิดีโอ</label>
                <div class="col-sm-10">
                    {{-- <input type="file" name="photo"
                        class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" accept="image/*"
                    value="{{$video->name}}"
                    onchange="loadFile(event)" > --}}
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">
                        แก้ไขรูปภาพ
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form method="post" action="{{ route('admin.picture.updateimage',[$picture->id]) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">แก้ไขรูปภาพ</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <img id="output" class="img-fluid" src="{{asset('image/Ex1poster.jpg')}}"
                                            alt="Responsive image" />
                                        <div class="col-sm-10">
                                            <input type="file" name="image"
                                                class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}"
                                                accept="image/*" onchange="loadFile(event)">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">ยกเลิก</button>
                                        <button type="submit" class="btn btn-success">บันทึก</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form method="post" action="{{ route('admin.picture.update',[$picture->id]) }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">หัวเรื่อง</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" value="{{$picture->title}}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">เนื่อหา</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="content"
                            rows="3">{{$picture->content}}</textarea>
                    </div>
                </div>
                <div class="form-group row py-3 ">

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
