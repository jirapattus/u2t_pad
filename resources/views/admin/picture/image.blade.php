@extends('layouts.admin.app')
@section('content')
<div class="container mt-4">
    <h2>เพิ่มรูปภาพ</h2>
    <form method="post" action="{{ route('admin.picture.addimage',$picture->id) }}"
        enctype="multipart/form-data">
        @csrf
     
            <div class="form-group">
                <input type="file" name="files[]" multiple class="form-control" accept="image/*">
                @if ($errors->has('files'))
                @foreach ($errors->get('files') as $error)
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $error }}</strong>
                </span>
                @endforeach
                @endif
            </div>
            <button type="submit" class="btn btn-success">บันทึก</button>
       
    </form>
    <div class="dropdown-divider"></div>
    <section class="portfolio">
        <div class="container">
            <div class="row portfolio-container" data-aos="fade-up" data-aos-easing="ease-in-out"
                data-aos-duration="500">
                @foreach ($picture1 as $key => $pictures)
                <div class="col-lg-4 col-md-6 portfolio-wrap filter-app">
                    <div class="portfolio-item box">
                        <img src="{{$pictures->path}}" class="img-fluid" alt="" style="width: 100%; height:100%;">
                        <div class="portfolio-info">
                            <div>
                                <form class="delete_form"
                                    action="{{ route('admin.picture.deleteimage',$pictures->id) }}" method="post">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger btn-circle" type="submit" name="" value="Delete"><i
                                            class="fas fa-trash"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section><!-- End Portfolio Section -->
</div>
@endsection
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).ready(function () {
        $('.delete_form').on('submit', function () {
            if (confirm("คุณต้องการลบข้อมูลหรือไม่")) {
                return true;
            } else {
                return false;
            }

        });
    });

</script>