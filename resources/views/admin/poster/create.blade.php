@extends('layouts.admin.app')
@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เพิ่มโปสเตอร์/ประชาสัมพันธ์</h6>
        </div>
        <img id="output" class="img-fluid" src="{{asset('image/Ex1poster.jpg')}}" alt="Responsive image" />
        <div class="container py-3">
            <form method="post" action="{{ route('admin.poster.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">รูปโปสเตอร์</label>
                    <div class="col-sm-10">
                        <input type="file" name="poster"
                            class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" accept="image/*"
                            onchange="loadFile(event)">
                    </div>
                </div>
                <div class="form-group row py-3 ">

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
    @endsection
