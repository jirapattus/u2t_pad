@extends('layouts.admin.app')
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">โปสเตอร์/ประชาสัมพันธ์</h1>
    <a href="{{route("admin.poster.create")}}" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i
            class="fas fa-download fa-sm text-white-50"></i> เพิ่มข้อมูล</a>
</div>
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รูปภาพ</th>
                            <th>จัดการ</th>
                            <th>.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($poster as $key => $posters)
                        <tr>
                            <td style="width:5%">{{ $key+1}}</td>
                            <td style="width:60%"><img src="{{$posters->path}}" style="height:300px; width:1000px" />
                            </td>
                            <td style="width:5%" align="center" ;>
                                <a href="{{ route('admin.poster.edit',[$posters->id]) }}"
                                    class="btn btn-warning btn-circle">
                                    <i class="fas fa-exclamation-triangle"></i>
                                </a>
                            </td>
                            <td style="width:5%" align="center";>
                                <form class="delete_form" action="{{route('admin.poster.destroy',$posters->id)}}"
                                    method="post">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger btn-circle" type="submit" name="" value="Delete"><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@endsection
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).ready(function(){
    $('.delete_form').on('submit',function(){
          if(confirm("คุณต้องการลบข้อมูลหรือไม่")){
                return true;
          }else{
                return false;
          }

    });
});
</script>
