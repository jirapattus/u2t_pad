@extends('layouts.admin.app')
@section('content')
<div class="container" data-aos="fade-up">

    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-4 d-none d-lg-block bg-register-image"></div>
                <div class="col-lg-8">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">สมัครสมาชิก</h1>
                        </div>
                        <form method="POST" class="user" action="{{ route('register') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label for="login">{{ __('คำนำหน้า') }}<br>&nbsp
                                        <input type="radio" name="tname" value="นาย" checked> นาย &nbsp
                                        <input type="radio" name="tname" value="นาง"> นาง&nbsp
                                        <input type="radio" name="tname" value="นางสาว"> นางสาว
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input id="fname" type="text"
                                        class="form-control form-control-user @error('fname') is-invalid @enderror"
                                        name="fname" value="{{ old('fname') }}" required autocomplete="name"
                                        placeholder="ชื่อ" autofocus>

                                    @error('fname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-sm-6">
                                    <input id="lname" type="text"
                                        class="form-control form-control-user @error('lname') is-invalid @enderror"
                                        name="lname" value="{{ old('lname') }}" required autocomplete="lname"
                                        placeholder="นามสกุล" autofocus>

                                    @error('lname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <input id="work" type="text"
                                    class="form-control form-control-user @error('email') is-invalid @enderror"
                                    name="work" value="" required 
                                    placeholder="ตำแหน่ง">

                            </div>
                            <div class="form-group">
                                <input id="email" type="email"
                                    class="form-control form-control-user @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email"
                                    placeholder="อีเมล">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input id="password" type="password"
                                        class="form-control form-control-user @error('password') is-invalid @enderror"
                                        name="password" placeholder="รหัสผ่าน" required autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-sm-6">
                                    <input id="password-confirm" type="password" class="form-control form-control-user"
                                        name="password_confirmation" required autocomplete="new-password"
                                        placeholder="ยืนยันรหัสผ่าน">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="file" name="image" class=" form-control-user"
                                    accept="image/x-png,image/gif,image/jpeg" />
                            </div>
                            <button class="btn btn-primary btn-user btn-block" name="" value="create profile"> ยืนยัน
                            </button>
                        </form>
                        <hr>
                        <div class="text-center">
                            <a class="small" href="forgot-password.html">Forgot Password?</a>
                        </div>
                        <div class="text-center">
                            <a class="small" href="login.html">Already have an account? Login!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
