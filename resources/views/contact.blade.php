@extends('layouts.app')

@section('content')
<main id="main">
    <!-- ======= Contact Section ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
            <h2>ทีมงาน U2T </h2>
            <ol>
            <li><a href="{{url('/')}}">หน้าแรก</a></li>
            <li><a>ทีมงาน</a></li>
          </ol>
            </div>

        </div>
        

    </section><!-- End Contact Section -->


</main>

<section class="team" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
      <div class="container">

        <div class="row">

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="image/person.png" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>ชื่อ-สกุล</h4>
                <span>ตำแหน่ง</span>
                <p>คำอธิบาย ขนาดรูป 600x600px</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="image/person.png" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>ชื่อ-สกุล</h4>
                <span>ตำแหน่ง</span>
                <p>คำอธิบาย ขนาดรูป 600x600px</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="image/person.png" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>ชื่อ-สกุล</h4>
                <span>ตำแหน่ง</span>
                <p>คำอธิบาย ขนาดรูป 600x600px</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="image/person.png" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>ชื่อ-สกุล</h4>
                <span>ตำแหน่ง</span>
                <p>คำอธิบาย ขนาดรูป 600x600px</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="image/person.png" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>ชื่อ-สกุล</h4>
                <span>ตำแหน่ง</span>
                <p>คำอธิบาย ขนาดรูป 600x600px</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="image/person.png" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>ชื่อ-สกุล</h4>
                <span>ตำแหน่ง</span>
                <p>คำอธิบาย ขนาดรูป 600x600px</p>
              </div>
            </div>
          </div>
          

        </div>

      </div>
    </section><!-- End Team Section -->
            
@endsection
    