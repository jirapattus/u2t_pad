@extends('layouts.admin.app')
@section('content')

    <!-- Page Wrapper -->
    <div id="wrapper">

              <!-- Begin Page Content -->
              <div class="container-fluid">

                  <!-- 404 Error Text -->
                  <div class="text-center">
                      <div class="error mx-auto" data-text="404">404</div>
                      <p class="lead text-gray-800 mb-5">Page Not Found</p>
                      <p class="text-gray-500 mb-0">ท่านได้เข้าสู่ระบบเรียบร้อยแล้ว</p>
                      <a href="{{ route('admin.dashboard.index')}}">&larr; กลับไปหน้าจัดการระบบ</a>
                  </div>

              </div>
              <!-- /.container-fluid -->

          </div>
          <!-- End of Main Content -->
@endsection