        <!-- ======= Header ======= -->
        <header id="header" class="fixed-top header-transparent" >
            <div class="container d-flex justify-content-between align-items-center">

                <div class="logo float-left">
                    <!-- <h1 class="text-light"><a href="index.html"><span>Moderna</span></a></h1> -->
                    <!-- Uncomment below if you prefer to use an image logo -->
                    <a href="{{ url('/') }}"><img src="{{ asset('assets/general/img/icon/icon.png') }}" alt="" class="img-fluid" style="width:55px; height:80px;"></a>
                </div>

                <nav class="nav-menu float-right d-none d-lg-block">
                    <ul>
                        <li class="active"><a href="{{url('/')}}">หน้าแรก</a></li>
                        <li><a href="{{url('/video')}}">วิดีโอ</a></li>
                        <li><a href="{{url('/activity')}}">รูปภาพกิจกรรม</a></li>
                        <li><a href="{{url('/store')}}">สินค้า</a></li>
                        <li><a href="{{url('/news')}}">ข่าว</a></li>
                        <li><a href="{{url('/contact')}}">ติดต่อ/ทีมงาน</a></li>
                        @guest
                        @else
                        <li class="drop-down">
                            <a href="">{{ Auth::user()->tname . "" . Auth::user()->fname. " " . Auth::user()->lname }}</a>
                            <ul>
                                <li><a href="#">
                                        <center><img src="{{ asset( Auth::user()->path ) }}"
                                                width="100px" height="100px" /></center>
                                    </a></li>
                                <li><a href="{{url('/dashboard')}}">จัดการระบบ</a></li>
                                <li><a href="#">แก้ไขข้อมูลส่วนตัว</a></li>
                                <li> <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                        <i class="material-icons"></i> {{ __('ล็อกเอาท์') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                    </ul>
                </nav><!-- .nav-menu -->
            </div>
        </div>
        </header><!-- End Header -->
 
