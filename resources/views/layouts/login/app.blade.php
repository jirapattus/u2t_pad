<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.login.head')
</head>
<body>
    <div id="app">
    @include('layouts.header')
    <!-- End Header Area -->
        <main class="py-0">
            @yield('content')
        </main>
    </div>
    @include('layouts.login.script')
</body>
</html>