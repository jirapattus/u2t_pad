@extends('layouts.app')

@section('content')
<main id="main">
    <!-- ======= Contact Section ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
            <h2>ข่าวประชาสัมพันธ์</h2>
            <ol>
            <li><a href="{{url('/')}}">หน้าแรก</a></li>
            <li><a>ข่าวประชาสัมพันธ์</a></li>
          </ol>
          
        </div>

            </div>

        </div>
    </section><!-- End Contact Section -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-8 entries">
          @foreach ($news as $key => $item) 
            <article class="entry">

              <div class="entry-img">
                <img src="{{ $item->path }}" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <p href="">{{ $item->title }}</p>
              </h2>

              <div class="entry-meta">
                <ul>
                  <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-single.html">{{ $item->id_admin }}</a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-single.html"><time datetime="2020-01-01">1 กันยายน 2564</time></a></li>
                </ul>
              </div>

              <div class="entry-content">
                <p>
                  {{Str::limit($item->content, 200, $end='.......')}}
                </p>
                <div class="read-more">
                  <a href="{{ route('news.show',[$item->id]) }}" class="btn btn-info float-right">ดูเพิ่มเติม</a>
                </div>
              </div>

            </article><!-- End blog entry -->
            @endforeach
          </div><!-- End blog entries list -->
        </div>

      </div>
    </section><!-- End Blog Section -->
</main>


    
@endsection
    