@extends('layouts.app')

@section('content')

<main id="main">
    <!-- ======= Contact Section ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>รายละเอียดข่าว</h2>
                <ol>
                    <li><a href="{{url('/')}}">หน้าแรก</a></li>
                    <li><a href="{{url('/news')}}">ข่าวประชาสัมพันธ์</a></li>
                    <li>รายละเอียดข่าว</li>
                </ol>
            </div>

        </div>
    </section><!-- End Contact Section -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <div class="row gy-4">
                <div class="col-lg-8">
                    <div class="portfolio-details-slider swiper">
                        <div class="swiper-wrapper align-items-center">

                            <div class="swiper-slide">
                                <img src="{{ $news->path}}" alt="">
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="portfolio-description">
                        <h2>{{ $news->title}}</h2>
                        <p>
                          {{ $news->content}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Portfolio Details Section -->

</main>



@endsection
