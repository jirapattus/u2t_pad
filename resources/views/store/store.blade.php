@extends('layouts.app')

@section('content')
<main id="main">
    <!-- ======= Contact Section ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
            <h2>สินค้า</h2>
            <ol>
            <li><a href="{{url('/')}}">หน้าแรก</a></li>
            <li><a>สินค้า</a></li>
          </ol>
            </div>

        </div>
    </section><!-- End Contact Section -->

<!-- ======= Portfolio Section ======= -->
<section class="portfolio">
      <div class="container">
        <div class="row portfolio-container" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
          @foreach ($store as $key => $item)
          <div class="col-lg-4 col-md-6 portfolio-wrap filter-app">
            <div class="portfolio-item">
              <img src="{{ $item->path }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h3>{{ $item->category }}</h3>
                <div>
                <a href="{{ route('store.show',[$item->id]) }}" title="ดูรายละเอียดเพิ่มเติม"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section><!-- End Portfolio Section -->

</main>



@endsection
    