@extends('layouts.app')

@section('content')

<main id="main">
    <!-- ======= Contact Section ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>รายละเอียดสินค้า</h2>
                <ol>
                    <li><a href="{{url('/')}}">หน้าแรก</a></li>
                    <li><a href="{{url('/store')}}">สินค้า</a></li>
                    <li>รายละเอียดสินค้า</li>
                </ol>
            </div>

        </div>
    </section><!-- End Contact Section -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">

            <div class="row gy-4">

                <div class="col-lg-8">
                    <div class="row" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
                        @foreach ($picture as $key => $pictures)
                        <div class="col-lg-12 col-md-6 portfolio-wrap filter-web py-2">
                            <div class="portfolio-item">
                                <a href="{{ $pictures->path }}" data-toggle="lightbox" data-max-width="600"><img
                                        src="{{$pictures->path}}" class="img-fluid" alt=""></a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="portfolio-info">
                        <h3>ข้อมูลสินค้า</h3>
                        <ul>
                            <p><strong>ราคา</strong>: {{$store->price }} บาท</p>
                            <li><strong>ประเภท</strong>: {{$store->category }}</li>
                            <li><strong>ติดต่อ</strong>: {{$store->phone }}</li>
                            <li><strong>facebook</strong>: <a href="{{$store->facebook }}">{{$store->facebook }}</a>
                            </li>
                            <li><strong>line</strong>: {{$store->line }} </li>
                        </ul>

                    </div>
                    <div class="portfolio-description">
                        <h2>รายละเอียดสินค้า</h2>
                        <p>
                            {{$store->details }}
                        </p>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Portfolio Details Section -->

</main>



@endsection
