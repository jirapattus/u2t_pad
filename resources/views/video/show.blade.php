@extends('layouts.app')
@section('content')
<!-- ======= Why Us Section ======= -->
<section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">
    <div class="container">

        <div class="section-title"></br>
            <h2>{{ $video->title }}</h2>
            <p>{{ $video->content }}</p>
        </div>
        <center>
            <div class="row">
                <div class="col-lg-12 video-box">
                    <img src="{{ $video->path }}" class="img-fluid" alt="">
                    <a href="{{ $video->linkvideo }}" class="venobox play-btn mb-4" data-vbtype="video"
                        data-autoplay="true"></a>
                </div>
            </div>
        </center>
    </div>
</section><!-- End Why Us Section -->
@endsection
