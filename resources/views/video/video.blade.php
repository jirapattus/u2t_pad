@extends('layouts.app')

@section('content')
<main id="main">
    <!-- ======= Contact Section ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>วิดีโอ</h2>
                <ol>
                    <li><a href="{{url('/')}}">หน้าแรก</a></li>
                    <li><a>วิดีโอ</a></li>
                </ol>
            </div>

        </div>
    </section><!-- End Contact Section -->

    <!-- ======= Why Us Section ======= -->
    @foreach ($video as $key => $videos)
    <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 video-box">
                    <img src="{{ $videos->path }}" class="img-fluid" alt="">
                    <a href="{{ $videos->linkvideo }}" class="venobox play-btn mb-4"
                        data-vbtype="video" data-autoplay="true"></a>
                </div>

                <div class="col-lg-6 d-flex flex-column justify-content-center p-5">

                    <div class="icon-box">
                        <h4 class="title"><a>{{ $videos->title }}</a></h4>
                        <p class="description">{{Str::limit($videos->content, 200, $end='.......')}}</p>
                    </div>  
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12 py-3">
                    <a href="{{ route('video.show',[$videos->id]) }}" class="btn btn-info float-right">ดูเพิ่มเติม</a>
                </div>
            </div>      
        </div>
    </section><!-- End Why Us Section -->
    @endforeach
</main>



@endsection
