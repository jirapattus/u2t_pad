@extends('layouts.app')

@section('content')
<main id="main">
    <section class="breadcrumbs">
    </section>
</main>
<!-- ======= Hero Section ======= -->
<div class="container">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach($poster as $key => $item)
            <li data-target="#carousel-example-generic" data-slide-to="{{$key}}"
                class="carousel-1 @if ($loop->first) active @endif "></li>
            @endforeach
        </ol>
        <div class="carousel-inner">
            @foreach($poster as $key => $item)
            <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
                <img class="d-block w-100" src="{{ asset($item->path) }}" alt="First slide">
            </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<main id="main">
    <!-- ======= Why Us Section ======= -->
    <section class="why-us section-bg " data-aos="fade-up" date-aos-delay="200">

        <div class="container">
            <div class="section-title"></br>
                <h2>วิดีโอ</h2>
                <p>-</p>
            </div>
            @foreach($video->take(3) as $key => $item)
            <div class="row">
                <div class="col-lg-4 video-box" data-aos="fade-up">
                    <img src="{{ $item->path }}" class="img-fluid" alt="">
                    <a href="{{ $item->linkvideo }}" class="venobox play-btn mb-4" data-vbtype="video"
                        data-autoplay="true"></a>
                </div>
                <div class="col-lg-8 d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon-box icon-box-cyan">
                        <h4 class="title">
                            <p href="">{{ $item->title }}</p>
                        </h4>
                        <p class="description"> {{Str::limit($item->content, 200, $end='.......')}}</p>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="row">
                <div class="col-lg-12 py-3">
                    <a href="{{url('/video')}}" class="btn btn-info float-right">ดูเพิ่มเติม</a>
                </div>
            </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Features Section ======= -->
    <section class="features">
        <div class="container">

            <div class="section-title">
                <h2>สินค้า</h2>
                <p></p>
            </div>
            <center>
                <div class="row" data-aos="fade-up">
                    @foreach($store->take(3) as $key => $item)
                    <div class="col-lg-4">
                        <div class="card" style="width: 20rem;">
                            <img src="{{ $item->path }}" class="card-img-top" alt="..."
                                style="width: auto; height:200px;">
                            <div class="card-body">
                                <h5 class="card-title">{{ $item->category }}</h5>
                                <p class="card-text">{{Str::limit($item->details, 200, $end='.......')}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </center>
            <div class="row">
                <div class="col-lg-12 py-3">
                    <a href="{{url('/store')}}" class="btn btn-info float-right">ดูเพิ่มเติม</a>
                </div>
            </div>
        </div>
    </section><!-- End Features Section -->
    <!-- ======= Why Us Section ======= -->
    <section class="why-us section-bg " data-aos="fade-up" date-aos-delay="200">

        <div class="container">
            <div class="section-title"></br>
                <h2>ข่าว/ประชาสัมพันธ์</h2>
                <p>-</p>
            </div>
            @foreach($news->take(3) as $key => $item)
            <div class="row">
                <div class="col-lg-2 mb-4 " data-aos="fade-up"><a href="{{ $item->path }}" data-toggle="lightbox"
                        data-max-width="600"><img src="{{ $item->path }}" class="img-fluid" alt="" class="img-fluid"></a>

                </div>
                <div class="col-lg-9 d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon-box icon-box-cyan">
                        <h4 class="title">
                            <p>{{ $item->title }}</p>
                        </h4>
                        <p class="description">{{Str::limit($item->content, 200, $end='.......')}}</p>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="row">
                <div class="col-lg-12 py-3">
                    <a href="{{url('/news')}}" class="btn btn-info float-right">ดูเพิ่มเติม</a>
                </div>
            </div>
        </div>
    </section><!-- End Why Us Section -->
    <section class="features">
        <div class="container">

            <div class="section-title">
                <h2>รูปภาพกิจกรรม</h2>
                <p></p>
            </div>
            <center>
                <div class="row row-cols-1 row-cols-md-3 g-4">
                    @foreach($picture->take(3) as $key => $item)
                    <div class="col">
                        <div class="card h-100">
                            <img src="{{ $item->path }}" class="card-img-top" alt="..."
                                style="width: auto; height:200px;">
                            <div class="card-body">
                                <h5 class="card-title">{{ $item->title }}</h5>
                            </div>
                        </div>
                    </div>
                    @endforeach 
                </div>
                <div class="row">
                    <div class="col-lg-12 py-3">
                        <a href="{{url('/activity')}}" class="btn btn-info float-right">ดูเพิ่มเติม</a>
                    </div>
                </div>
            </center>
        </div>
    </section><!-- End Features Section -->
</main>
@endsection
