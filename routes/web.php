<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () { return view('welcome'); });
Route::get('/', 'WelcomeController@index');
Route::get('/test', function () { return view('test'); });
Auth::routes();
Route::get('/dashboard', 'AdminDashboardController@index')->name('admin.dashboard.index');

Route::get('/home', 'HomeController@index')->name('home');
// admin////////////////////////////////////////////////
// poster
Route::get('/admin/poster', 'AdminPosterController@index')->name('admin.poster.index');
Route::get('/admin/poster/create', 'AdminPosterController@create')->name('admin.poster.create');
Route::post('/admin/poster/store', 'AdminPosterController@store')->name('admin.poster.store');
Route::get('/admin/poster/edit/{id}', 'AdminPosterController@edit')->name('admin.poster.edit');
Route::post('/admin/poster/update/{id}', 'AdminPosterController@update')->name('admin.poster.update');
Route::delete('/admin/poster/destroy/{id}', 'AdminPosterController@destroy')->name('admin.poster.destroy');

// video
Route::get('/admin/video', 'AdminVideoController@index')->name('admin.video.index');
Route::get('/admin/video/create', 'AdminVideoController@create')->name('admin.video.create');
Route::post('/admin/video/store', 'AdminVideoController@store')->name('admin.video.store');
Route::get('/admin/video/edit/{id}', 'AdminVideoController@edit')->name('admin.video.edit');
Route::post('/admin/video/update/{id}', 'AdminVideoController@update')->name('admin.video.update');
Route::post('/admin/video/updateimage/{id}', 'AdminVideoController@updateimage')->name('admin.video.updateimage');
Route::delete('/admin/video/destroy/{id}', 'AdminVideoController@destroy')->name('admin.video.destroy');

//picture
Route::get('/admin/picture', 'AdminPictureController@index')->name('admin.picture.index');
Route::get('/admin/picture/create', 'AdminPictureController@create')->name('admin.picture.create');
Route::post('/admin/picture/store', 'AdminPictureController@store')->name('admin.picture.store');
Route::get('/admin/picture//edit/{id}', 'AdminPictureController@edit')->name('admin.picture.edit');
Route::post('/admin/picture/update/{id}', 'AdminPictureController@update')->name('admin.picture.update');
Route::post('/admin/picture/updateimage/{id}', 'AdminPictureController@updateimage')->name('admin.picture.updateimage');
Route::delete('/admin/picture/destroy/{id}', 'AdminPictureController@destroy')->name('admin.picture.destroy');

Route::get('/admin/picture/image/{id}', 'AdminPictureController@image')->name('admin.picture.image');
Route::post('/admin/picture/addimage/{id}', 'AdminPictureController@addimage')->name('admin.picture.addimage');
Route::delete('/admin/picture/deleteimage/{id}', 'AdminPictureController@deleteimage')->name('admin.picture.deleteimage');

// store
Route::get('/admin/store', 'AdminStoreController@index')->name('admin.store.index');
Route::get('/admin/store/create', 'AdminStoreController@create')->name('admin.store.create');
Route::post('/admin/store/store', 'AdminStoreController@store')->name('admin.store.store');
Route::get('/admin/store//edit/{id}', 'AdminStoreController@edit')->name('admin.store.edit');
Route::post('/admin/store/update/{id}', 'AdminStoreController@update')->name('admin.store.update');
Route::post('/admin/store/updateimage/{id}', 'AdminStoreController@updateimage')->name('admin.store.updateimage');
Route::delete('/admin/store/destroy/{id}', 'AdminStoreController@destroy')->name('admin.store.destroy');

Route::get('/admin/store/image/{id}', 'AdminStoreController@image1')->name('admin.store.image1');
Route::post('/admin/store/addimage/{id}', 'AdminStoreController@addimage')->name('admin.store.addimage');
Route::delete('/admin/store/deleteimage/{id}', 'AdminStoreController@deleteimage')->name('admin.store.deleteimage');

// news
Route::get('/admin/news', 'AdminNewsController@index')->name('admin.news.index');
Route::get('/admin/news/create', 'AdminNewsController@create')->name('admin.news.create');
Route::post('/admin/news/store', 'AdminNewsController@store')->name('admin.news.store');
Route::get('/admin/news//edit/{id}', 'AdminNewsController@edit')->name('admin.news.edit');
Route::post('/admin/news/update/{id}', 'AdminNewsController@update')->name('admin.news.update');
Route::post('/admin/news/updateimage/{id}', 'AdminNewsController@updateimage')->name('admin.news.updateimage');
Route::delete('/admin/news/destroy/{id}', 'AdminNewsController@destroy')->name('admin.news.destroy');

// user
Route::get('/admin/user', 'AdminUserController@index')->name('admin.user.index');
Route::get('/admin/user/edit/{id}', 'AdminUserController@edit')->name('admin.user.edit');
Route::delete('/admin/user/destroy/{id}', 'AdminUserController@destroy')->name('admin.user.destroy');

// fontend
// video
Route::get('/video', 'VideoController@index')->name('video.index');
Route::get('/video/show/{id}', 'VideoController@show')->name('video.show');

// activity
Route::get('/activity', 'ActivityPicturescontroller@index')->name('activity.index');
Route::get('/activity/show/{id}', 'ActivityPicturescontroller@show')->name('activity.show');

// store
Route::get('/store', 'Storecontroller@index')->name('store.index');
Route::get('/store/show/{id}', 'Storecontroller@show')->name('store.show');

// news
Route::get('/news', 'Newscontroller@index')->name('news.index');
Route::get('/news/show/{id}', 'Newscontroller@show')->name('news.show');

Route::get('/contact', 'Contactcontroller@index');


